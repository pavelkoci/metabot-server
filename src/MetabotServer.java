import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.concurrent.Executor;

public class MetabotServer {
    public static void main(String[] args) throws Exception {
        HttpServer server = HttpServer.create(new InetSocketAddress(9898), 0);

        HttpContext c = server.createContext("/title", new TitleHandler());
        c.getFilters().add(new ParameterFilter());

        server.setExecutor(null);
        server.start();
    }
}