import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import redis.clients.jedis.Jedis;
import sun.tools.jconsole.Version;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.*;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TitleHandler implements HttpHandler {
    Connection conn;
    Statement st;
    ResultSet rs;
    PreparedStatement pst;
    Jedis redis;

    Jedis jedis = new Jedis("localhost");

    public TitleHandler() {
        this.conn = null;
        this.st = null;
        this.rs = null;

        this.redis = new Jedis("localhost");

        String url = "jdbc:postgresql://localhost/stv";
        String user = "stv";
        String password = "";

        try {
            conn = DriverManager.getConnection(url, user, password);
            st = conn.createStatement();

            String stm = "SELECT id, name FROM wiki_title WHERE id = ?";
            pst = conn.prepareStatement(stm);
        }
        catch (SQLException ex) {
            Logger l = Logger.getLogger(Version.class.getName());
            l.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        InputStream is = httpExchange.getRequestBody();
        is.read();

        Headers h = httpExchange.getResponseHeaders();
        h.add("Connection", "Keep-Alive");

        Map<String, Object> params = (Map<String, Object>)httpExchange.getAttribute("parameters");
        String id = params.get("id").toString();

        String response = this.redis.get(id);
        if (response == null) {
            response = "";
            try {
                pst.setInt(1, Integer.parseInt(id));
                rs = pst.executeQuery();

                while (rs.next()) {
                    response += rs.getString(1) + ',' + rs.getString(2);
                }
            }
            catch (SQLException ex) {
                Logger l = Logger.getLogger(Version.class.getName());
                l.log(Level.SEVERE, ex.getMessage(), ex);
            }

            this.redis.set(id, response);
        }

        httpExchange.sendResponseHeaders(200, response.length());

        OutputStream s = httpExchange.getResponseBody();
        s.write(response.getBytes());
        s.close();
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            if (this.rs != null) {
                rs.close();
            }

            if (st != null) {
                st.close();
            }

            if (conn != null) {
                conn.close();
            }
        }
        catch (SQLException ex) {
            Logger l = Logger.getLogger(Version.class.getName());
            l.log(Level.SEVERE, ex.getMessage(), ex);
        }

        super.finalize();
    }
}
